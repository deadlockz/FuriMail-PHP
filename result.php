<!DOCTYPE html>
<html lang="en">
<head>
	<?php
	require_once('lib/FuriMail.php'); 
	$result = FuriMail('mailto@---examplewhere.org');
	?>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php if ($result == 'ok') 
		print('<meta http-equiv="refresh" content="3;url=http://---examplewhere.org/result.php">'."\n"); 
	?>
	<title>Result</title>
</head>
<body>
	<?php
		if ($result == 'error') {
	?>
		<h2>Message <b>not</b> sended!</h2>
		An error is happend. try it again with a smaler image or less quality.
		<a href="form.html">[try again]</a>
	<?php } elseif ($result == 'ok') { ?>
		<h2>sending ...</h2>
		please wait.
	<?php } else { ?>
		<h2>Ready.</h2>
		Thank you. <a href="form.html">[make new message]</a>
	<?php } ?>
</body>
</html>
