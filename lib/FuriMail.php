<?php
setlocale(LC_TIME, 'de_DE.utf8');
require_once('FuriMailerClass.php');

define('SEC_SALT', 'abc123');
define('RAWPATH', './raw/');
define('FROM_MAIL', 'mailfrom@---examplewhere.org');
define('IMAGE_SMALLER_WIDTH', 200);

function cleaner($field) {
	$result = '';
	if (isset($_REQUEST[$field])) $result = trim(strip_tags($_REQUEST[$field]));
	return $result;
}


function storeAsJpg($fileIn, $fileOut, $outWidth) {
	list($width, $height, $imageType) = getimagesize($fileIn);
	
	switch ($imageType) {
		case 1:  $src = imagecreatefromgif($fileIn); break;
		case 2:  $src = imagecreatefromjpeg($fileIn); break;
		case 3:  $src = imagecreatefrompng($fileIn); break;
		default: return false;
	}
	
	$ratio = (float) $height / $width;
	$outHeight = (int) ($ratio * $outWidth);
	$tmp = imagecreatetruecolor($outWidth, $outHeight);
	imagecopyresampled($tmp, $src, 0, 0, 0, 0, $outWidth, $outHeight, $width, $height);
	imagejpeg($tmp, $fileOut);
	return true;
}

function FuriMail($to) {
	$body = sprintf( ''
		."Name............: \t%s\n"
		."eMail...........: \t%s\n"
		."Rights..........: \t%s\n"
		."Place...........: \t%s\n"
		."Date............: \t%s\n"
		."Time............: \t%s\n"
		."Type............: \t%s\n"
		."Message:\n%s",
		cleaner('Name'),
		cleaner('Email'),
		cleaner('Rights'),
		cleaner('Place'),
		cleaner('Date'),
		cleaner('Time'),
		cleaner('Type'),
		cleaner('Message')
	);
	
	$hash = md5(time()."-".sprintf("%s",SEC_SALT)."-".mt_rand());
	
	$html = ''
		. '<!DOCTYPE html>'
		. '<html><head><meta content="text/html; charset=UTF-8" http-equiv="Content-Type"></head>'
		. '<body>'
		. '<a href="http://---examplewhere.org/result.php?hash='.$hash.'">'
		.'<font color="green">message is ok (not implemented)</font></a><br /><br />'
		. '<a href="http://---examplewhere.org/result.php?hash='.$hash.'&del=1">'
		. '<font color="red">remove message (not implemented)</font></a><br />'
		. '<br /><pre>'
		. $body
		. '</pre></body></html>';

	$result = ' ';

	if(isset($_REQUEST['sendNow'])) {
		$expected = array('attachment1', 'attachment2', 'attachment3');
		$farray = array();
		$i=1;
		foreach ($expected as $ex) {
			if (empty($_FILES[$ex]['tmp_name'])) continue;
			
			$fileName = basename($_FILES[$ex]['name']);
			$tempName = $_FILES[$ex]['tmp_name'];
			$check = getimagesize($tempName);
			if($check !== false) {
				$farray[] = array('name' => $fileName, 'mime' => $check['mime'], 'temp' => $tempName);
				$outFile = sprintf("%s%s_%d_.jpg", RAWPATH, $hash, $i);
				storeAsJpg($tempName, $outFile, IMAGE_SMALLER_WIDTH);
				$i++;
			}
		}
		// make bots slow ?!
		sleep(5);
		
		$result = 'error';
		
		$ok = FuriMailerClass::send(
			FROM_MAIL, 
			$to, 
			'Message from examplewhere website',
			$body,
			$html,
			$farray
		);
		$result = ($ok)? 'ok' : 'error';
	}
	return $result;
}
