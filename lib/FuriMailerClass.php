<?php
/**
 * This class is the Mail helper
 * 
 * @return Mail
 */
class FuriMailerClass {
	
	/**
	 * send a textonly mail or html+txt+attachment
	 * 
	 * @param string $from (email-address)
	 * @param string $to (email-address)
	 * @param string $subject
	 * @param string $body
	 * @param string $html or NULL (html ignored with no attachment!!)
	 * @param array $attachmentArray if this array is NULL or size 0, no attachment is send
	 * @return bool
	 */
	public static function send($from, $to, $subject, $body, $html = NULL, $attachmentArray = NULL) {
		$nl = PHP_EOL;
		$subject = '=?utf-8?B?'.base64_encode($subject).'?=';
		
		if (is_null($attachmentArray) OR isset($attachmentArray[0]) === false) {
			$header = ''
				. 'From: ' . $from . $nl
				. 'MIME-Version: 1.0' . $nl
				. 'Content-Type: text/plain; charset=UTF-8' . $nl
				. 'Content-Transfer-Encoding: 7bit';
			return mail($to, $subject, $body, $header);
		}

		$separator  = '='.md5(time()).'=';
		$separator2 = '==xy0815html'.md5(time()+4711);
		
		$header = '';
		if (is_null($html)) {
			$header = ''
				. 'From: ' . $from . $nl
				. 'MIME-Version: 1.0' . $nl
				. 'Content-Type: multipart/mixed;' . $nl
				. ' boundary="' . $separator . '"';
			$body = ''
				. 'This is a multi-part message in MIME format.' . $nl
				. '--' . $separator . $nl
				. 'Content-Type: text/plain; charset=UTF-8; format=flowed' . $nl
				. 'Content-Transfer-Encoding: 7bit' . $nl
				. $nl . $body . $nl;
		} else {
			$header = ''
				. 'From: ' . $from . $nl
				. 'MIME-Version: 1.0' . $nl
				. 'Content-Type: multipart/mixed;' . $nl
				. ' boundary="' . $separator . '"';
			$body = ''
				. 'This is a multi-part message in MIME format.' . $nl
				. '--' . $separator . $nl
				. 'Content-Type: multipart/alternative;' . $nl
				. ' boundary="' . $separator2 . '"' . $nl
				. $nl
				. '--' . $separator2 . $nl
				. 'Content-Type: text/plain; charset=UTF-8; format=flowed' . $nl
				. 'Content-Transfer-Encoding: 7bit' . $nl
				. $nl . $body . $nl;
			$html = ''
				. '--' . $separator2 . $nl
				. 'Content-Type: text/html; charset=UTF-8' . $nl
				. 'Content-Transfer-Encoding: 7bit' . $nl
				. $nl . $html . $nl
				. '--' . $separator2 . '--' . $nl;
		}
		
		if (is_null($html) === false) {
		}
		$attachments = '';
		foreach ($attachmentArray as $attachment) {
			// attachment
			$content = file_get_contents($attachment['temp']);
			$content = chunk_split(base64_encode($content));
			$attachments .= ''
				. $nl . '--' . $separator . $nl
				. 'Content-Type: ' . $attachment['mime'] . ';' . $nl
				. ' name="' . $attachment['name'] . '"' . $nl
				. 'Content-Transfer-Encoding: base64' . $nl
				. 'Content-Disposition: attachment;' . $nl
				. ' filename="' . $attachment['name'] . '"' . $nl . $nl
				. $content . $nl;
		}
		$attachments .= '--' . $separator . '--';
		return mail($to, $subject, $body.$html.$attachments, $header);
	}
	
	/**
	 * UNUSED
	 * 
	 * convert an utf-8 into an iso format (and make additional html-entities)
	 * 
	 * @param string $str
	 * @param bool $html
	 * 
	 * @return string
	 */
	public static function toIso($str, $html = false) {
		$search = array(
			'UTF-8',
			'ä',
			'ö',
			'ü',
			'Ä',
			'Ö',
			'Ü',
			'ß'
		);
		$replace = array(
			'ISO-8859-1',
			'&auml;',
			'&ouml;',
			'&uuml;',
			'&Auml;',
			'&Ouml;',
			'&Uuml;',
			'&szlig;'
		);
		if($html === true) $str = str_replace($search, $replace, $str);
		return iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $str);
	}
}
