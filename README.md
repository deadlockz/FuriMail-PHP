# Form Upload Resize Image Mail Attachment with PHP (FuriMail PHP)

This is an example to send a single text+html+someAttachmentImages from
an apache/php web/mailserver. There is a html form to insert text and
upload files. The uploaded files (jpg, png, gif) will be attached to the
email and will be resized and stored in the `raw/` folder.

## german chars and utf8

german chars in body, subject etc. are tested and works well.

## html inline images

Html inline images do not work!

## Note

This is a reduced example code - it is not tested but the basic of an
well tested webapp. Feel free to use it but be warned: do not make your
website to a spam util or someting like that.
